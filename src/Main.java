import com.company.*;
import com.company.utilities.Substitution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Main {

    public void ShiftRun() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("Исходный текст: ");
        String text =sc.nextLine();
        System.out.println("Ключ: ");
        int key = sc.nextInt();
        String res = Shift.encrypt(text,key);
        System.out.println("Шифротекст:\n");
        System.out.println(res);
        System.out.println();
        System.out.println(Shift.decrypt(res,key));
    }

    public void ReplaceRun() throws Exception{
        /*
а б
б в
в а
         */
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размерность алфавита");
        int n = sc.nextInt();
        sc.nextLine();
        Substitution st = new Substitution(n);
        System.out.println("Ключ: ");
        for (int i = 0 ; i < n ; i++){
            String from = sc.next();
            String to = sc.next();
            st.put(from,to);
        }
        System.out.println("Исходный текст: ");
        String text =sc.nextLine();
        String res = Replace.encrypt(text,st);
        System.out.println("Шифротекст:\n");
        System.out.println(res);
        System.out.println();
        System.out.println(Replace.decrypt(res,st));

    }

    public void PermutRun() throws Exception{
        Scanner sc = new Scanner(System.in);
        System.out.println("Исходный текст: ");
        String text =sc.nextLine();
        System.out.println("Размер перестановки: ");
        int n = sc.nextInt();
        Substitution st = new Substitution(n);
        System.out.println("Перестановка: ");
        for (int i = 0 ; i < n ; i++){
            int p = sc.nextInt();
            st.put(Integer.toString(i+1),Integer.toString(p));
        }
        String res = Permutation.encrypt(text,st);
        System.out.println("Шифротекст:\n");
        System.out.println(res);
        System.out.println();
        System.out.println(Permutation.decrypt(res,st));
    }

    public void GammaRun() throws Exception{
        Scanner sc = new Scanner(System.in);
        System.out.println("Исходный текст: ");
        String text =sc.nextLine();
        System.out.println("Гамма: ");
        String gamma = sc.nextLine();
        String res = Gamma.encrypt(text,gamma);
        System.out.println("Шифротекст:\n");
        System.out.println(res);
        System.out.println();
        System.out.println(Gamma.decrypt(res,gamma));
    }

    public void GridRun() throws Exception{
    /*
       0 0 0 0
       1 1 0 0
       0 0 0 0
       1 0 1 0
     */
        Scanner sc = new Scanner(System.in);
        System.out.println("Исходный текст: ");
        String text =sc.nextLine();
        System.out.println("Размер решетки: ");
        int n = sc.nextInt();
        boolean[][] grid = new boolean[n][n];
        System.out.println("Решетка: ");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (sc.nextInt() == 1)
                    grid[i][j] = true;
            }
        }
        String res = Grid.encrypt(text,grid);
        System.out.println("Шифротекст:\n");
        System.out.println(res);
        System.out.println();
        System.out.println(Grid.decrypt(res,grid));
    }

    public void VigenereRun() throws Exception{
        Scanner sc = new Scanner(System.in);
        System.out.println("Исходный текст: ");
        String text =sc.nextLine();
        System.out.println("Ключ: ");
        String key = sc.next();
        String res = Vigenere.encrypt(text,key);
        System.out.println("Шифротекст:\n");
        System.out.println(res);
        System.out.println();
        System.out.println(Vigenere.decrypt(res,key));
    }

    public void PleiferRun(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Исходный текст: ");
        String text =sc.nextLine();
        System.out.println("Ключ: ");
        String key = sc.next();
        String res = Pleifer.encrypt(text,key);
        System.out.println("Шифротекст:\n");
        System.out.println(res);
        System.out.println();
        System.out.println(Pleifer.decrypt(res,key));
    }

    public void run() throws Exception {
        Scanner sc = new Scanner(System.in);
//        prepareStr();
        HackVigenere.crack(sc.nextLine());
        //PleiferRun();
    }

    public static void prepareStr(){
        Scanner sc = new Scanner(System.in);
        String text = "";
        while(sc.hasNextLine()){
            text+=sc.nextLine();
        }
        int[] tt = (text.chars().filter(k->((k>='а'&&k<='я')||(k>='А'&&k<='Я'))).map(k->Character.toUpperCase(k)).toArray());
        text="";
        for (int j = 0; j < tt.length; j++) {
            text+=(char)tt[j];
        }
        System.out.println(text);
    }


    public static void main(String[] args) throws Exception {
        new Main().run();
    }
}
