package com.company.utilities;

import java.util.HashMap;

public class Substitution {
    int size;
    HashMap<String,String> subst;

    public int size(){return size;}

    public Substitution(int size) {
        this.size = size;
        subst = new HashMap<>();
    }

    public int put(String from,String to){
        if (subst.size() == size && subst.containsKey(from))
            return 0;
        subst.put(from,to);
        return 1;
    }

    public String get(String from){
        return subst.get(from);
    }

    public String get(char sym){return this.get(""+sym);}

    public Substitution reverse() {
        Substitution result = new Substitution(this.size);
        for (String a:subst.keySet()){
            result.put(subst.get(a),a);
        }
        return result;
    }

    public  Substitution multiply(Substitution mul)    {
        if (this.size != mul.size)
        return null;
        Substitution result = new Substitution(this.size);
        for (String a : subst.keySet()){
            if (mul.subst.containsKey(subst.get(a)))
                result.put(a,mul.subst.get(subst.get(a)));
        }
        return result;
    }

}
