package com.company;

import java.util.Arrays;

public class Vigenere {

    private static String prepare(String str){
        StringBuilder result = new StringBuilder();
        for (int i = 0 ; i < str.length() ; i++) {
            if (Character.isAlphabetic(str.charAt(i))){
                result.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return result.toString();
    }

    public static String encrypt(String value,String key){
        value = prepare(value);
        //prepare key
        key = prepare(key);
        StringBuilder res = new StringBuilder(value.length());
        for (int i = 0; i < value.length(); i++) {
            char c = (char) (((value.charAt(i) +key.charAt(i%key.length()) - 2*'А')%32)+'А');
            res.append(c);
        }
        return res.toString();
    }

    public static String decrypt(String value,String key){
        key = prepare(key);
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < value.length(); i++) {
            char c = (char) (((value.charAt(i) -key.charAt(i%key.length())+32)%32)+'А');
            res.append(c);
        }
        return res.toString();
    }

}
