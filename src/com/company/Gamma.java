package com.company;

public class Gamma {

    private static String prepare(String str){
        StringBuilder result = new StringBuilder();
        for (int i = 0 ; i < str.length() ; i++) {
            if (Character.isAlphabetic(str.charAt(i))){
                result.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return result.toString();
    }

    public static String encrypt(String value,String gamma){
        StringBuilder st = new StringBuilder();
        for (int i = 0; i < value.length(); i++) {
                char sym  = value.charAt(i);
                char gamSym = gamma.charAt(i%gamma.length());
                char res = (char) (sym^gamSym);
                st.append(res);
            }
        return st.toString();
    }

    public static String decrypt(String value,String gamma){
        return encrypt(value,gamma);
    }

}
