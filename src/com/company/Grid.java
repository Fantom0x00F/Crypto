package com.company;

public class Grid {

    public static boolean[][] turn( boolean grid[][]){
        boolean [][] res = new boolean[grid.length][grid.length];
        for (int i = 0 ; i < grid.length ; i++){
            for (int j = 0; j < grid.length; j++) {
                res[i][j] = grid[grid.length-j-1][i];
            }
        }
        return res;
    }

    //check grid
    public static boolean check(boolean grid[][]){
        int n = grid.length;
        for (int i = 0; i < n; i++) {
            if (grid[i].length!=n)
                return false;
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j]){
                    if (grid[n-j-1][i] || grid[n-i-1][n-j-1] || grid[j][n-i-1]){
                        return false;
                    }
                }
                else{
                    int q = ((grid[n-j-1][i])?1:0);
                    q += ((grid[n-i-1][n-j-1])?1:0);
                    q += ((grid[j][n-i-1])?1:0);
                    if (q!=1)
                        return false;
                }
            }
        }
        return true;
    }

    public static String encrypt(String value,boolean grid[][]) throws Exception {
        if (!check(grid))
            throw new Exception("Incorrect grid");
        int index = 0,len = value.length();
        int n = grid.length;
        StringBuilder res = new StringBuilder();
        while(index<len) {
            char[][] arr = new char[n][n];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                    arr[i][j] = 'A';
            for (int q = 0; q < 4; q++) {
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        if (grid[i][j] && index < len)
                            arr[i][j] = value.charAt(index++);
                grid = turn(grid);
            }
            for (int i = 0; i < n; i++) {
                res.append(arr[i]);
            }
        }
        return res.toString();
    }

    public static String decrypt(String value,boolean grid[][]) throws Exception {
        if (!check(grid))
            throw new Exception("Incorrect grid");
        int n = grid.length;
        int quan = n*n;
        if (value.length()%quan!=0)
            throw new Exception("Incorrect length of value");
        StringBuilder res = new StringBuilder();
        for (int q = 0; q < value.length() / quan; q++) {
            String s = value.substring(q*quan,(q+1)*quan);
            for (int w = 0; w < 4; w++) {
                for (int i = 0; i < n; i++) {
                    for (int j = 0; j < n; j++) {
                        if (grid[i][j])
                            res.append(s.charAt(i*n+j));
                    }
                }
                grid = turn(grid);
            }
        }
        return res.toString();
    }
}
