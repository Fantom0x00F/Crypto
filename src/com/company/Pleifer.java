package com.company;
import java.util.HashSet;
import java.util.HashMap;

public class Pleifer {

    private final static int S = 4;
    private final static int C = 8;

    public static String encrypt(String value,String key){
        return algo(value,key,true);
    }

    public static String decrypt(String value,String key){
        return algo(value,key,false);
    }

    private static String prepare(String str){
        StringBuilder result = new StringBuilder();
        for (int i = 0 ; i < str.length() ; i++) {
            if (Character.isAlphabetic(str.charAt(i))){
                result.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return result.toString();
    }

    private static HashMap<Character,Pair> getIndex(String key){
        HashMap<Character, Pair> res = new HashMap<>();
        char[][] table = getTable(key);
        for (int i = 0; i < S; i++) {
            for (int j = 0; j < C; j++) {
                res.put(table[i][j],new Pair(i,j));
            }
        }
        return res;
    }

    private static char[][] getTable(String key){
        key = prepare(key);
        char[][] table = new char[S][C];
        HashSet<Character> alph = new HashSet<>();
        int index = 0;
        for (int i = 0; i < key.length(); i++) {
            char c = key.charAt(i);
            if (!alph.contains(c)){
                table[index/C][index%C] = c;
                alph.add(c);
                index++;
            }
        }
        for (char с = 'А'; с <='Я' ; с++) {
            if (!alph.contains(с)){
                table[index/C][index%C] = с;
                alph.add(с);
                index++;
            }
        }
        return table;
    }

    private static String algo(String value,String key,boolean mode){
        value = prepare(value);
        if (value.length()%2!=0)
            value+='А';
        key = prepare(key);
        HashMap<Character,Pair> index =getIndex(key);
        char[][] table = getTable(key);
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < value.length() / 2; i++) {
            char first = value.charAt(i*2);
            char second= value.charAt(i*2+1);
            int s1 = index.get(first).x,c1 = index.get(first).y,s2 = index.get(second).x,c2 = index.get(second).y;
            int nS1,nC1,nS2,nC2;
            if (s1 == s2){
                nS1 = s1;
                nS2 = s2;
                if (mode){
                    nC1 = (c1+1)%C;
                    nC2 = (c2+1)%C;
                }
                else
                {
                    nC1 = (c1-1+C)%C;
                    nC2 = (c2-1+C)%C;
                }
            }
            else if (c1 == c2){
                nC1 = c1;
                nC2 = c2;
                if (mode){
                    nS1 = (s1+1)%S;
                    nS2 = (s2+1)%S;
                }
                else{
                    nS1 = (s1-1+S)%S;
                    nS2 = (s2-1+S)%S;
                }
            }
            else{
                nS1 = s1;
                nC1 = c2;
                nS2 = s2;
                nC2 = c1;
            }
            res.append(table[nS1][nC1]);
            res.append(table[nS2][nC2]);
        }
        return res.toString();
    }

    private static class Pair{
        int x;
        int y;

        public Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "["+x+","+y+"]";
        }
    }
}
