package com.company;

import com.company.utilities.Substitution;

public class Replace {

    private static String prepare(String str){
        StringBuilder result = new StringBuilder();
        for (int i = 0 ; i < str.length() ; i++) {
            if (Character.isAlphabetic(str.charAt(i))){
                result.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return result.toString();
    }

    public static String encrypt(String value,Substitution st) {
        value = prepare(value);
        StringBuilder result = new StringBuilder();
        for (int i = 0 ; i < value.length() ; i++) {
            result.append(st.get(value.charAt(i)));
        }
        return result.toString();
    }

    public static String decrypt(String value,Substitution st){
        StringBuilder result = new StringBuilder();
        Substitution reversed = st.reverse();
        for (int i = 0 ; i < value.length(); i++){
            result.append(reversed.get(value.charAt(i)));
        }
        return result.toString();
    }

}
