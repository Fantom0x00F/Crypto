package com.company;

import com.company.utilities.Substitution;

public class Permutation {

    private static String prepare(String str){
        StringBuilder result = new StringBuilder();
        for (int i = 0 ; i < str.length() ; i++) {
            if (Character.isAlphabetic(str.charAt(i))){
                result.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return result.toString();
    }

    public static String encrypt(String value,Substitution st) {
        String preparedValue = prepare(value);
        while (preparedValue.length() % st.size() !=0){
            preparedValue+='A';
        }
        Substitution reversed = st.reverse();
        StringBuilder result = new StringBuilder();
        int countOfBlocks = preparedValue.length()/st.size();
        for (int j = 0 ; j < countOfBlocks ; j++)
            for (int i = 0 ; i < st.size(); i++) {
                int pos = Integer.parseInt(reversed.get(Integer.toString(i+1)));
                result.append(preparedValue.charAt(pos+j*st.size()-1));
            }
        return result.toString();
    }

    public static String decrypt(String value,Substitution st){
        StringBuilder result = new StringBuilder();
        int countOfBlocks = value.length()/st.size();
        for (int j = 0 ; j< countOfBlocks ; j++)
            for (int i = 0 ; i < st.size(); i++){
                int pos = Integer.parseInt(st.get(Integer.toString(i+1)));
                result.append(value.charAt(pos+j*st.size()-1));
            }
        return result.toString();
    }

}
