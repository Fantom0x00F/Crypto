package com.company;

public class Shift {

    private static String prepare(String str){
        StringBuilder result = new StringBuilder();
        for (int i = 0 ; i < str.length() ; i++) {
            if (Character.isAlphabetic(str.charAt(i))){
                result.append(Character.toUpperCase(str.charAt(i)));
            }
        }
        return result.toString();
    }

    public static String encrypt(String value, int key) throws Exception {
        if (key>=32)
            throw new Exception("Key must be less 32");
        StringBuilder result = new StringBuilder();

        String upperCaseValue = prepare(value);
        for (int  i = 0 ; i < upperCaseValue.length() ; i++) {
            char pos = upperCaseValue.charAt(i);
            char res = (char) (((pos +key - 'А')%32)+'А');
            result.append(res);
        }
        return result.toString();
    }

    public static String decrypt(String value,int key) throws Exception {
        if (key>=32)
            throw new Exception("Key must be less 32");
        StringBuilder result = new StringBuilder();
        for (int  i = 0 ; i < value.length() ; i++) {
            char pos = value.charAt(i);
            char res = (char) (((pos -key - 'А'+32)%32)+'А');
            result.append(res);
        }
        return result.toString();
    }

}
