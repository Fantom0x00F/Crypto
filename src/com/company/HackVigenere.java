package com.company;

import java.util.*;

public class HackVigenere {

    private static final double minGreat = 1.06;

    private static final int diagramLen = 3;

    private static double[][] bigramP=
            {
                    {2,12,35,8,14,7,6,15,7,7,19,27,19,45,5,11,
                            26,31,27,3,1,10,6,7,10,1,0,0,0,2,6,9},
                    {5,0,0,0,0,9,1,0,6,0,0,6,0,2,21,0,
                            8,1,0,6,0,0,0,0,0,1,0,11,0,0,0,2},
                    {35,1,5,3,3,32,0,2,17,0,7,10,3,9,58,6,
                            6,19,6,7,0,1,1,2,4,1,0,18,1,2,0,3},
                    {7,0,0,0,3,3,0,0,5,0,1,5,0,1,50,0,
                            7,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0},
                    {25,0,3,1,1,29,1,1,13,0,1,5,1,13,22,3,
                            6,8,1,10,0,0,1,1,1,0,0,5,1,0,0,1},
                    {2,9,18,11,27,7,5,10,6,15,13,35,24,63,7,16,
                            39,37,33,3,1,8,3,7,3,3,0,0,0,1,1,2},
                    {5,1,0,0,6,12,0,0,5,0,0,0,0,6,0,0,
                            0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                    {35,1,71,1,5,3,0,0,4,0,2,1,2,9,9,1,
                            3,1,0,2,0,0,0,0,0,0,0,4,0,0,0,4},
                    {4,6,22,5,10,21,2,23,19,11,19,21,20,32,8,13,
                            11,29,29,3,1,17,3,11,1,1,0,0,0,1,3,17},
                    {1,1,4,1,3,0,1,2,4,0,5,1,2,7,9,7,
                            3,10,2,0,0,0,1,3,2,0,0,0,0,0,0,0},
                    {24,1,4,1,0,4,1,1,26,0,1,4,1,2,66,2,
                            10,3,7,10,0,0,1,0,0,0,0,0,0,0,0,0},
                    {25,1,1,1,1,33,2,1,36,0,1,2,1,8,30,2,
                            0,3,1,6,0,4,0,1,0,0,0,3,20,0,4,9},
                    {18,2,4,1,1,21,1,2,23,0,3,1,3,7,19,5,
                            2,5,3,9,1,0,0,2,0,0,0,5,1,1,0,3},
                    {54,1,2,3,3,34,0,0,58,0,3,0,1,24,67,2,
                            1,9,9,7,1,0,5,2,0,0,0,36,3,0,0,5},
                    {1,28,84,32,47,15,7,18,12,29,19,41,38,30,9,18,
                            43,50,39,3,2,5,2,12,4,3,0,0,0,2,3,2},
                    {7,0,0,0,0,15,0,0,4,0,0,9,0,1,46,0,
                            41,1,0,6,0,0,0,0,0,0,0,2,0,0,0,2},

                    {55,1,4,4,3,37,3,1,24,0,3,1,3,7,56,2,
                            1,5,9,16,0,1,1,1,2,0,0,8,3,0,0,5},
                    {8,1,7,1,2,25,0,0,6,0,40,13,3,9,27,11,
                            4,11,82,6,0,1,1,2,2,0,0,1,8,0,0,17},
                    {35,1,27,1,3,31,0,1,28,0,5,1,1,11,56,4,
                            26,18,2,10,0,0,0,1,0,0,0,11,21,0,0,4},
                    {1,4,4,4,11,2,6,3,2,0,8,5,5,5,1,5,
                            7,14,7,0,0,1,0,8,3,2,0,0,0,0,9,1},
                    {2,0,0,0,0,2,0,0,2,0,0,0,0,0,1,0,
                            1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                    {4,1,4,1,3,1,0,2,3,0,4,3,3,4,18,5,
                            3,4,2,2,1,0,0,1,0,0,0,0,0,0,0,0},
                    {3,0,0,0,0,7,0,0,10,0,2,0,0,0,1,0,
                            0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0},
                    {12,0,0,0,0,23,0,0,13,0,2,0,0,6,0,0,
                            0,0,7,1,0,0,0,0,1,0,0,0,1,0,0,0},
                    {5,0,0,0,0,11,0,0,14,0,1,2,0,2,2,0,
                            0,0,0,1,0,0,0,0,0,0,0,0,1,0,0,0},
                    {3,0,0,0,0,8,0,0,6,0,0,0,0,1,0,0,
                            0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0},
                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
                    {0,1,9,1,3,12,0,2,4,7,3,6,6,3,2,10,
                            3,9,4,1,0,16,0,1,2,0,0,0,0,0,0,0},
                    {0,2,4,1,1,2,0,2,2,0,6,0,3,13,2,4,
                            1,11,3,0,0,0,0,1,4,0,0,0,0,1,3,1},
                    {0,0,0,0,0,0,0,0,0,0,1,0,0,1,0,0,
                            0,1,9,0,0,0,0,0,0,0,0,0,0,0,0,0},
                    {0,2,1,2,1,0,0,3,1,0,1,0,1,1,1,3,
                            1,1,7,0,0,0,1,1,0,4,0,0,0,0,0,0},
                    {1,3,9,1,3,3,1,5,3,2,3,3,4,6,3,6,
                            3,6,10,0,0,2,1,4,1,1,0,0,0,1,1,1}
            };


    private static HashMap<Character,Double> rusTable= new HashMap<>();
    static
    {
        rusTable.put('О',9.28);
        rusTable.put('А',8.66);
        rusTable.put('Е',8.10);
        rusTable.put('И',7.45);
        rusTable.put('Н',6.35);
        rusTable.put('Т',6.30);
        rusTable.put('Р',5.53);
        rusTable.put('С',5.45);
        rusTable.put('Л',4.32);
        rusTable.put('В',4.19);
        rusTable.put('К',3.47);
        rusTable.put('П',3.35);
        rusTable.put('М',3.29);
        rusTable.put('У',2.90);
        rusTable.put('Д',2.56);
        rusTable.put('Я',2.22);
        rusTable.put('Ы',2.11);
        rusTable.put('Ь',1.90);
        rusTable.put('З',1.81);
        rusTable.put('Б',1.51);
        rusTable.put('Г',1.41);
        rusTable.put('Й',1.31);
        rusTable.put('Ч',1.27);
        rusTable.put('Ю',1.03);
        rusTable.put('Х',0.92);
        rusTable.put('Ж',0.78);
        rusTable.put('Ш',0.77);
        rusTable.put('Ц',0.52);
        rusTable.put('Щ',0.49);
        rusTable.put('Ф',0.40);
        rusTable.put('Э',0.17);
        rusTable.put('Ъ',0.04);

        int summ = 0;
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 32; j++) {
                if (bigramP[i].length==32)
                summ+=bigramP[i][j];
                else
                    System.err.println(i);
            }
        }
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < 32; j++) {
                bigramP[i][j]/=summ;
            }
        }
    }

    private static int gcd(int a,int b){
        return (b==0)?a:gcd(b,a%b);
    }

    private static class Pair implements Comparable<Pair>{
        int ind;
        int value;

        public Pair(int ind, int value) {
            this.ind = ind;
            this.value = value;
        }

        @Override
        public int compareTo(Pair o) {
            return Integer.compare(this.value,o.value);
        }
    }

    //Автокорреляционный метод
    private static List<Integer> getCorellLen(String value){
        int len = value.length();
        double[]  vals = new double[len/2];
        for (int q = 1; q < len / 2; q++) {
            int count = 0;
            for (int i = 0; i < value.length(); i++) {
                if (value.charAt(i) == value.charAt((i+q)%value.length()))
                    count++;
            }
            vals[q] = count*1.0/value.length();
        }
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < vals.length; i++) {
            double candidate = vals[i]*minGreat;
            if (Arrays.stream(vals).allMatch(d-> candidate>d)){
                res.add(i);
            }
        }
        return res;
    }

    //Метод индекса совпадения
    private static List<Integer> getIS(String value){
        int len = value.length();
        double[] vals = new double[len/4];
        for (int q= 1; q < len / 4; q++) {//длина кодового слова
            double[] ISi = new double[q];
            int lenBlock = value.length()/q;
            for (int j = 1; j <= q; j++) {//номер блока
                ISi[j-1] = 0;
                int[] chLet = new int[32];
                for (int i = j; i < value.length(); i += q) {//проход по блоку
                    chLet[value.charAt(i) - 'А']++;
                }
                for (int i = 0; i < 32; i++) {
                    ISi[j-1]+=(chLet[i]*(chLet[i]-1))/(lenBlock*(lenBlock-1));
                }
            }
            vals[q] = Arrays.stream(ISi).sum()/q;
        }
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < vals.length; i++) {
            double candidate = vals[i]*minGreat;
            if (Arrays.stream(vals).allMatch(d-> candidate>d)){
                res.add(i);
            }
        }
        return res;
    }

    //Метод Касики
    private static List<Integer> getKasLen(String value){
        int[] nods = new int[value.length()];
        List<Integer> repeatCount = new ArrayList<>();
        for (int i = 0; i < value.length() - diagramLen + 1; i++) {
            String diagram = value.substring(i,i+diagramLen);
            for (int j = i+1; j < value.length()-diagramLen+1; j++) {
                if (diagram.equals(value.substring(j,j+diagramLen))){
                    repeatCount.add(j-i);
                }
            }
        }
        for (int i = 0; i < repeatCount.size(); i++) {
            for (int j = i+1; j < repeatCount.size(); j++) {
                nods[gcd(repeatCount.get(i),repeatCount.get(j))]++;
            }
        }
        nods[0]=0;
        List<Pair> nodsSort = new ArrayList<>();
        for (int i = 0; i < nods.length; i++) {
            if(nods[i]!=0){
                nodsSort.add(new Pair(i,nods[i]));
            }
        }
        Collections.sort(nodsSort);
        List<Integer> res = new ArrayList<>();
        for (int i = nodsSort.size()-1; i >=nodsSort.size()-10 && i>=0; i--) {
            res.add(nodsSort.get(i).ind);
        }
        return res;
    }

    //Критерий согласия Пирсона
    private static double getKhi2(ArrayList<Double> hm){
        double res = 0;
        for (char i = 'А'; i <='Я' ; i++) {
            res+=(rusTable.get(i) - hm.get(i-'А'))*(rusTable.get(i) - hm.get(i-'А'))/32;
        }
        return res;
    }

    //Криптоанализ шифра сдвига
    private static int getSm(String str){
        ArrayList<Double> lk = new ArrayList<Double>();
        for (int i = 0; i < 32; i++) {
            lk.add(0.0);
        }
        for (int i = 0; i < str.length(); i++) {
            int ind = str.charAt(i)-'А';
            double d = lk.get(ind);
            lk.set(ind,d+1);
        }
        for (int i = 0; i < 32; i++) {
            lk.set(i,lk.get(i)/str.length());
        }
        double minKni = getKhi2(lk);
        int res = 0;
        for (int i = 1; i < 32; i++) {
            lk.add(lk.remove(0));
            double Khi = getKhi2(lk);
            if (Khi<minKni){
                minKni = Khi;
                res = i;
            }
        }
        return res;
    }

    //Долгий метод путем перестановки алфавита
    private static String getKey(String value,int keyLength){
        int cI = 400000;
        int countIteration = cI;
        Random rand = new Random();
        StringBuilder possibleKey = new StringBuilder(keyLength);
        double har = 0;
        for (int i = 0; i < keyLength; i++) {
            possibleKey.append('А');
        }
        while(countIteration>0){
            int ind = rand.nextInt(keyLength);
            char before = possibleKey.charAt(ind);
            possibleKey.setCharAt(ind, (char) (rand.nextInt(32)+'А'));
            String reversed = Vigenere.decrypt(value,possibleKey.toString());
            int[][] bigrammCount = new int[32][32];
            for (int i = 0; i < reversed.length() - 1; i++) {
                bigrammCount[reversed.charAt(i)-'А'][reversed.charAt(i+1)-'А']++;
            }
            double newHar = 0;
            for (int i = 0; i < 32; i++) {
                for (int j = 0; j < 32; j++) {
                    newHar+=bigrammCount[i][j]*bigramP[i][j];
                }
            }
            if (newHar>har){
                countIteration=cI;
                har = newHar;
            }
            else{
                possibleKey.setCharAt(ind, before);
            }
            countIteration--;
        }
        return possibleKey.toString();
    }

    public static void crack(String value) {
        Scanner sc = new Scanner(System.in);
        String text = value.replaceAll(" ","");
        List<Integer> candidates = new ArrayList<>();
        candidates.addAll(getCorellLen(text));
        candidates.addAll(getIS(text));
        candidates.addAll(getKasLen(text));
        Collections.sort(candidates);
        System.out.println("Список кандидатов на длинну кодового слова:");
        System.out.print("Автокорреляционный метод: ");
        for (int i:getCorellLen(text)){
            System.out.print(i+" ");
        }
        System.out.println();
        System.out.print("Метод индекса совпадения: ");
        for (int i:getIS(text)){
            System.out.print(i+" ");
        }
        System.out.println();
        System.out.print("Метод Касики: ");
        for (int i:getKasLen(text)){
            System.out.print(i+" ");
        }
        System.out.println();
        System.out.println("Введите длинну кодового слова (<=0 exit):");
        int keyLength = sc.nextInt();
        while(true) {
            if (keyLength<=0)
                break;
            String[] columns = new String[keyLength + 1];
            for (int i = 0; i < keyLength; i++) {
                StringBuilder colI = new StringBuilder();
                for (int j = i; j < text.length(); j += keyLength) {
                    colI.append(text.charAt(j));
                }
                columns[i] = colI.toString();
            }
            String key = "";
            for (int i = 0; i < keyLength; i++) {
                key += (char) (getSm(columns[i]) + 'А');
            }
            System.out.print("Вероятное кодовое слово: ");
            System.out.println(key);
            System.out.println(Vigenere.decrypt(text, key));
            System.out.println("Введите длинну кодового слова (<=0 exit):");
            keyLength = sc.nextInt();
        }
    }

}
